/*
 * ttynvt stress tool
 */
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>

#include <sys/ioctl.h>
#include <asm/termbits.h>

#include "lib/nvt_log.h"


#define HELP \
    "Usage: nvttest -dqr [-n loops] [-t threads]\n"

static const char *dev = "/dev/ttyNVT0";

typedef struct {
    int             nthread;
    int             ithread;
    int             nloop;
    bool            opt_rand_delay;
} topts_t;

static void    *_worker(void *arg)
{
    topts_t        *topts = arg;
    int             iloop;
    int             fd, err, nw;
    struct termios  ios;

    for (iloop = 0; iloop < topts->nloop; iloop++)
    {
        nvt_log(LOG_INFO, "Thread %d/%d run %d/%d\n",
                topts->ithread, topts->nthread, iloop + 1, topts->nloop);
        {
            fd = open(dev, O_RDWR);
            if (fd < 0)
            {
                nvt_log(LOG_ERR, "Open '%s' failed: %m\n", dev);
                break;
            }

            memset(&ios, 0, sizeof(ios));
            err = ioctl(fd, TCGETS, &ios);
            if (err != 0)
            {
                nvt_log(LOG_ERR, "ioctl(TCGETS) failed: %m\n");
                break;
            }
            err = ioctl(fd, TCSETS, &ios);
            if (err != 0)
            {
                nvt_log(LOG_ERR, "ioctl(TCSETS) failed: %m\n");
                break;
            }
            close(fd);
        }

        {
            int             i, len;
            char            buf[1024];

            for (i = 0; i < (int)sizeof(buf); i++)
                buf[i] = '0' + i % 10;

            fd = open(dev, O_RDWR);
            if (fd < 0)
            {
                nvt_log(LOG_ERR, "Open '%s' failed: %m\n", dev);
                break;
            }

            for (i = 0; i < 10; i++)
            {
                len = rand() & 0xff;
                nw = write(fd, buf, len);
                if (nw < 0)
                {
                    nvt_log(LOG_ERR, "write failed: %m\n");
                    break;
                }
                if (nw < len)
                {
                    nvt_log(LOG_ERR, "write short: %u/%u\n", nw, len);
                    break;
                }
            }
            close(fd);
        }

        if (topts->opt_rand_delay)
            usleep(rand() & 0xfff);
    }

    return NULL;
}

int main(int argc, char **argv)
{
    int             err;
    int             opt;
    int             ithr, nthr;
    topts_t         topts = { }, *popts;
    pthread_t      *ptids;
    int             debug, quiet;

    debug = quiet = 0;
    nthr = 1;
    topts.nloop = 1;

    while ((opt = getopt(argc, argv, "dn:qrt:")) != -1)
    {
        switch (opt)
        {
        default:               /* '?' */
            fprintf(stderr, HELP);
            exit(EXIT_FAILURE);
        case 'd':
            debug += 1;
            break;
        case 'n':
            topts.nloop = atoi(optarg);
            break;
        case 'q':
            quiet = 1;
            break;
        case 'r':
            topts.opt_rand_delay = true;
            break;
        case 't':
            nthr = atoi(optarg);
            break;
        }
    }

    nvt_log_level(quiet, debug);
    nvt_log_dest(NVT_LOG_STDOUT);

    topts.nthread = nthr;

    ptids = malloc(nthr * sizeof(pthread_t));

    for (ithr = 0; ithr < nthr; ithr++)
    {
        nvt_log(LOG_INFO, "Create worker thread %d/%d\n", ithr + 1, nthr);

        popts = malloc(sizeof(topts_t));
        *popts = topts;
        popts->ithread = ithr + 1;

        err = pthread_create(&ptids[ithr], NULL, _worker, popts);
        if (err)
        {
            nvt_log(LOG_ERR, "Failed to create thread %d/%d\n", ithr, nthr);
            break;
        }
    }

    for (ithr = 0; ithr < nthr; ithr++)
    {
        if (ptids[ithr] == 0)
            break;
        pthread_join(ptids[ithr], NULL);
        nvt_log(LOG_INFO, "Joined worker thread %d/%d\n", ithr + 1, nthr);
    }

    return 0;
}
