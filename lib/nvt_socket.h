/*
 * Socket functions
 */
#ifndef NVT_SOCKET_H
#define NVT_SOCKET_H

int             socket_create_client(const char *addr);

int             socket_create_server(const char *addr);
int             socket_create_server_client(int sfd);

#endif /* NVT_SOCKET_H */
