/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet RFC-2217 handling - client
 */
#include "telnet.h"
#include "telnet_param.h"

void telnet_rfc2217_handle_opt(tn_ctx_t * tcc, int opt,
                               unsigned char *pu, int nd)
{
    unsigned int    sub;

    sub = pu[0];
    switch (sub)
    {
    default:
        break;
    case 100:                  /* SIGNATURE */
        telnet_set_opt(tcc, opt, 0, "LT-NVT", 6);
        break;
    case 106:                  /* NOTIFY-LINESTATE */
        break;
    case 107:                  /* NOTIFY-MODEMSTATE */
        if (tcc->srv_ms)
            tcc->srv_ms(tcc->cctx, pu[1]);
        break;
    case 108:                  /* FLOWCONTROL-SUSPEND */
    case 109:                  /* FLOWCONTROL-RESUME */
        break;
    }
}

void telnet_rfc2217_cfg(tn_ctx_t * tcc, int cmd, const void *val, int nd)
{
    if (!tcc->mode_rfc2217)
        return;

    telnet_set_opt(tcc, TNO_CPCO, cmd, val, nd);
}

void telnet_rfc2217_ctl(tn_ctx_t * tcc, unsigned int val)
{
    unsigned char   byte = val;

    if (!tcc->mode_rfc2217)
        return;

    telnet_set_opt(tcc, TNO_CPCO, TNS_SET_CONTROL, &byte, 1);
}
