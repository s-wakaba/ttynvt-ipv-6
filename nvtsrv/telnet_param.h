/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet parameters
 */
#ifndef TELNET_PARAM_H
#define TELNET_PARAM_H


/*
 * Telnet commands
 */

#define TNC_F0_SE   (0xf0)      /* End of subnegotiation parameters */
#define TNC_F1_NOP  (0xf1)      /* No operation                     */
#define TNC_F2_MARK (0xf2)      /* The data stream portion of a Synch.
                                   This should always be accompanied
                                   by a TCP Urgent notification. */
#define TNC_F3_BRK  (0xf3)      /* NVT character BRK.               */
#define TNC_F4_IP   (0xf4)      /* The function IP (interrupt process) */
#define TNC_F5_AO   (0xf5)      /* The function AO (abort output)   */
#define TNC_F6_AYT  (0xf6)      /* The function AYT (are you there) */
#define TNC_F7_EC   (0xf7)      /* The function EC (erase charceter) */
#define TNC_F8_EL   (0xf8)      /* The function EL (erase line)     */
#define TNC_F9_GA   (0xf9)      /* The GA signal (go ahead)         */
#define TNC_FA_SB   (0xfa)      /* Subnegotiation begin             */
#define TNC_FB_WILL (0xfb)      /* Indicates the desire to begin
                                   performing, or confirmation that
                                   you are now performing, the
                                   indicated option. */
#define TNC_FC_WONT (0xfc)      /* Indicates the refusal to perform,
                                   or continue performing, the
                                   indicated option. */
#define TNC_FD_DO   (0xfd)      /* Indicates the request that the
                                   other party perform, or
                                   confirmation that you are expecting
                                   the other party to perform, the
                                   indicated option. */
#define TNC_FE_DONT (0xfe)      /* Indicates the demand that the
                                   other party stop performing,
                                   or confirmation that you are no
                                   longer expecting the other party
                                   to perform, the indicated option. */
#define TNC_FF_IAC  (0xff)      /* Interpret as Command / data byte */


/*
 * Telnet options
 */

/*
 * https://www.iana.org/assignments/telnet-options/telnet-options.xhtml
 *
 *   0  00  Binary Transmission                 [RFC856]
 *   1  01  Echo                                [RFC857]
 *   2  02  Reconnection                        [DDN Protocol Handbook, ... ]
 *   3  03  Suppress Go Ahead                   [RFC858]
 *   4  04  Approx Message Size Negotiation     ["The Ethernet, ..." ]
 *   5  05  Status                              [RFC859]
 *   6  06  Timing Mark                         [RFC860]
 *   7  07  Remote Controlled Trans and Echo    [RFC726]
 *   8  08  Output Line Width                   [DDN Protocol Handbook, ... ]
 *   9  09  Output Page Size                    [DDN Protocol Handbook, ... ]
 *  10  0a  Output Carriage-Return Disposition  [RFC652]
 *  11  0b  Output Horizontal Tab Stops         [RFC653]
 *  12  0c  Output Horizontal Tab Disposition   [RFC654]
 *  13  0d  Output Formfeed Disposition         [RFC655]
 *  14  0e  Output Vertical Tabstops            [RFC656]
 *  15  0f  Output Vertical Tab Disposition     [RFC657]
 *  16  10  Output Linefeed Disposition         [RFC658]
 *  17  11  Extended ASCII                      [RFC698]
 *  18  12  Logout                              [RFC727]
 *  19  13  Byte Macro                          [RFC735]
 *  20  14  Data Entry Terminal                 [RFC1043][RFC732]
 *  21  15  SUPDUP                              [RFC736][RFC734]
 *  22  16  SUPDUP Output                       [RFC749]
 *  23  17  Send Location                       [RFC779]
 *  24  18  Terminal Type                       [RFC1091]
 *  25  19  End of Record                       [RFC885]
 *  26  1a  TACACS User Identification          [RFC927]
 *  27  1b  Output Marking                      [RFC933]
 *  28  1c  Terminal Location Number            [RFC946]
 *  29  1d  Telnet 3270 Regime                  [RFC1041]
 *  30  1e  X.3 PAD                             [RFC1053]
 *  31  1f  Negotiate About Window Size         [RFC1073]
 *  32  20  Terminal Speed                      [RFC1079]
 *  33  21  Remote Flow Control                 [RFC1372]
 *  34  22  Linemode                            [RFC1184]
 *  35  23  X Display Location                  [RFC1096]
 *  36  24  Environment Option                  [RFC1408]
 *  37  25  Authentication Option               [RFC2941]
 *  38  26  Encryption Option                   [RFC2946]
 *  39  27  New Environment Option              [RFC1572]
 *  40  28  TN3270E                             [RFC2355]
 *  41  29  XAUTH                               [Rob_Earhart]
 *  42  2a  CHARSET                             [RFC2066]
 *  43  2b  Telnet Remote Serial Port (RSP)     [Robert_Barnes]
 *  44  2c  Com Port Control Option             [RFC2217]
 *  45  2d  Telnet Suppress Local Echo          [Wirt_Atmar]
 *  46  2e  Telnet Start TLS                    [Michael_Boe]
 *  47  2f  KERMIT                              [RFC2840]
 *  48  30  SEND-URL                            [David_Croft]
 *  49  31  FORWARD_X                           [Jeffrey_Altman]
 *  50-137  32-89  Unassigned                   [IANA]
 * 138  8a  TELOPT PRAGMA LOGON                 [Steve_McGregory]
 * 139  8b  TELOPT SSPI LOGON                   [Steve_McGregory]
 * 140  8c  TELOPT PRAGMA HEARTBEAT             [Steve_McGregory]
 * 141-254  8d-fe  Unassigned
 * 255  ff  Extended-Options-List               [RFC861]
*/

#define TNO_BINARY   0          /* 00  Binary Transmission          */
#define TNO_ECHO     1          /* 01  Echo                         */
#define TNO_SUPP_GA  3          /* 03  Suppress Go Ahead            */
#define TNO_LOGOUT  18          /* 12  Logout                       */
#define TNO_TTYP    24          /* 18  Terminal Type                */
#define TNO_NAWS    31          /* 1f  Negotiate About Window Size  */
#define TNO_NEO     39          /* 27  New Environment Option       */
#define TNO_CPCO    44          /* 2c  RFC 2217                     */
#define TNO_KERMIT  47          /* 2f  Kermit                       */

#endif /* TELNET_PARAM_H */
